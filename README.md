# RectangleFrame-in-Java



import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class RectangleFrame extends JFrame
{

   /**
     Constructs the frame
    */
    public RectangleFrame()
    {
     // the panel that draws the rectangle
     rectPanel = new RectanglePanel();
     
     // add panel to content pane
     getContentPane().add(rectPanel, BorderLayoyt.CENTER);
     
     createControlPanel();
     
     pack();
  }
  
  private void createControlPanel()
  { 
     // the text fields for entering the - x and -y coordinates
     Final JTextField xField = new JTextField(5);
     Final JTextField yField = new JTextField(5);;
     
     // the button to move the rectangle
     JButton moveButton = new JButton("Move");
     
     class MoveButtonListener implements ActionListener
     {
       public void actionPerformed(ActionEvent event)
       {
          int x = Integer.parseInt(xField.getText());
          int y = Integer.parseInt(yField.getText());
          rectPanel.setLocation(x, y);
       }
     };
     
     ActionListener listener = new MoveButtonListener();
     moveButton.addActionListener(listener);
     
     // the labels for labelling the text field
     JLabel xLabel = new JLabel("x = ");
     JLabel yLabel = new JLabel("y = ");
     
     // the panel for holding the user interface components
     JPanel controlPanel = new JPanel();
     
     controlPanel.add(xLabel);
     controlPanel.add(xField)
     controlPanel.add(yLabel);
     controlPanel.add(yField);
     controlPanel.add(moveButton);
     
     getContentPane().add(controlPanel, BorderLayout.SOUTH);
   }
   
   private RectanglePanel rectPanel;
 }
     
     
              
